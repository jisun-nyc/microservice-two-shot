import React, { useEffect, useState } from 'react';


function HatsList() {
    const [hats, setHats] = useState([]);

    async function loadHats() {
        const response = await fetch('http://localhost:8090/api/hats/');
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        } else {
            console.error(response);
        }
    }
    useEffect(() => {
        loadHats();
    }, []);


    const handleDelete = async (id) => {

        const updatedHats = await fetch(`http://localhost:8090/api/hats/${id}`, {method: "Delete"});
        if (updatedHats.ok) {
            const result = hats.filter(hat => hat.id != id);
            setHats(result);
        }
    }

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Style Name</th>
                    <th>Fabric</th>
                    <th>Color</th>
                    <th>Picture Url</th>
                    <th>Location</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(hat => {
                    return (
                    <tr key={hat.href}>
                        <td>{ hat.style_name }</td>
                        <td>{ hat.fabric }</td>
                        <td>{ hat.color }</td>
                        <td>{ hat.picture_url }</td>
                        <td>{ hat.location }</td>
                        <td>
                            <button onClick={() => handleDelete(hat.id)}>
                                Delete
                            </button>
                        </td>
                    </tr>
                );
            })}
            </tbody>
        </table>
    );
}

export default HatsList;
